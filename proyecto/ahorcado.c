#include<stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <windows.h>

typedef struct jugador* usuario;
void setNombre(usuario j, char nom[30]);
void setPalabra(usuario j, char pal[20]);
void setPuntaje(usuario j, int punt);
void setLongitud(usuario j, int lon);
char* getNombre(usuario j);
char* getPalabra(usuario j);
int getPuntaje(usuario j);
int getLongitud(usuario j);
void destruirJugador(usuario j);
usuario inicilizarJuador();
usuario cargarJugador();
void cargarJugadores(usuario j[], int cant);
int consultaDeEleccion(usuario j[], int cant, int auxElejir);
int cargarAdivinar(int ele);
void inicializarPalabra(usuario j, int lon);
void elejirPalabra(usuario j[], int cant, int ele, int adiv);
void mostrarPalabra(usuario j, int lon);
int busquedaLetra(char palaElegida[20], int lon, char busco, int i);
void reemplazarLetras(int posicion, char palaElegida[20], char palaAdivinar[20]);
int buscarYReemplazar(usuario j[], int ele, int adiv);
int contarVacios(char palabra[20], int lon);
void juego(usuario j[], int ele, int adiv, int errores);
void cargarPuntaje(usuario j);
void mostrarPuntaje(usuario j[]);
void puntajeFinal(usuario j[]);
int deseaJugar();
void transformarAMinuscula(char palabra[], int lon);
void nombreJuego();
void ahorcado0();
void ahorcado1();
void ahorcado2();
void ahorcado3();
void ahorcado4();
void ahorcado5();

struct jugador{
	char nombre[30];
    char palabra[20];
    int puntaje;
    int longitud;

};


int main()
{

	system("COLOR C0 ");	// si quiere volver al color normal borrar esta linea....
    usuario jug[2];
    int elejir=-1;
    int adivinar=-1;
    int errores=0;
    int op=0;

    cargarJugadores(jug, 2);

    do{
        system("cls");
        elejir=consultaDeEleccion(jug, 2, elejir);
        system("cls");
        adivinar=cargarAdivinar(elejir);
        system("cls");
        elejirPalabra(jug, 2, elejir, adivinar);
        system("cls");
        juego(jug, elejir, adivinar, errores);
        op=deseaJugar();
        puntajeFinal(jug);
    }while(op != 2);

    return 0;
}

void setNombre(usuario j, char nom[30]){
    strcpy(j->nombre, nom);
}
void setPalabra(usuario j, char pal[20]){
    strcpy(j->palabra, pal);
}
void setPuntaje(usuario j, int punt){
    j->puntaje=punt;
}
void setLongitud(usuario j, int lon){
    j->longitud=lon;
}

char* getNombre(usuario j){
    return j->nombre;
}
char* getPalabra(usuario j){
    return j->palabra;
}
int getPuntaje(usuario j){
    return j->puntaje;
}
int getLongitud(usuario j){
    return j->longitud;
}

void destruirJugador(usuario j){
    free(j);
}

usuario inicilizarJuador(){

    usuario j=malloc(sizeof(struct jugador));
    setNombre(j, " ");
    setPalabra(j, " ");
    setPuntaje(j, -1);

    return j;
}
usuario cargarJugador(){

    char auxNombre[30]=" ";

    usuario j=malloc(sizeof(struct jugador));
    printf("Ingresar nombre:  ");
    fflush(stdin);
    gets(auxNombre);
    setNombre(j, auxNombre);
    setPuntaje(j, 0);

    return j;
}
void cargarJugadores(usuario j[], int cant){
	int i=0;
    for( i=0; i<cant; i++){
        printf("            JUGADOR NUMERO %d\n\n", i+1);
        j[i]=cargarJugador();
        system("cls");
    }
}

int consultaDeEleccion(usuario j[], int cant, int auxElejir){

    char auxchar;

    do{
            system("cls");
    printf("          �Que jugador elije la palabra?\n\n");
    printf("                 1- %s\n", getNombre(j[0]));
    printf("                 2- %s\n\n", getNombre(j[1]));
    printf("            OPCION: ");
    scanf("%c", &auxchar);
        switch(auxchar){
            case '1': auxElejir=0;
                     break;
            case '2': auxElejir=1;
                    break;
            default: printf("\n\n NO ES UN DATO VALIDO\n");
                    auxElejir = -1;
                    break;
    }
    }while(auxElejir == -1);

    return auxElejir;
}
int cargarAdivinar(int ele){
    int aux=-1;
    if(ele==0){
        aux=1;
    }else{
        aux=0;
    }
    return aux;
}
void inicializarPalabra(usuario j, int lon){
	int i=0;
    for( i=0; i<lon; i++){
        j->palabra[i]= '_';
    }

}
void elejirPalabra(usuario j[], int cant, int ele, int adiv){
    char palab[20]=" ";
    int auxlong=-1;

    printf("\n\n      %s Escriba una palabra por favor:  ", getNombre(j[ele]));
    fflush(stdin);
    gets(palab);
    setPalabra(j[ele], palab);
    auxlong=strlen(palab);
    setLongitud(j[adiv],auxlong);
    inicializarPalabra(j[adiv], getLongitud(j[adiv]));
    transformarAMinuscula(getPalabra(j[ele]), getLongitud(j[adiv]));
}
void mostrarPalabra(usuario j, int lon){
    printf("            ");
    int k=0;
    for( k=0; k<lon; k++){
    printf(" %c ", j->palabra[k]);
    }
}
int busquedaLetra(char palaElegida[20], int lon, char busco, int i){

    int encontrado=0;
    int posicion=-1;

    while((i<lon) && (encontrado==0)){
        if(palaElegida[i]==busco){
            encontrado=1;
            posicion=i;
        }
        i++;
    }
    return posicion;
}
void reemplazarLetras(int posicion, char palaElegida[20], char palaAdivinar[20]){
    char aux;
    aux=palaElegida[posicion];
    palaAdivinar[posicion]= aux;
}
int buscarYReemplazar(usuario j[], int ele, int adiv){

    char letra;
    int pos=-1;
    int aux=-1;

    printf("\n\n      DIGA UNA LETRA: ");
    fflush(stdin);
    scanf("%c",&letra);
    do{
        pos = busquedaLetra(j[ele]->palabra, j[adiv]->longitud, letra, pos+1);
        if(pos != -1){
            reemplazarLetras(pos, j[ele]->palabra, j[adiv]->palabra);
            aux=1;
        }
    }while(pos != -1);

    return aux;
}
int contarVacios(char palabra[20], int lon){

    int aux=0;
	int i=0;
    for( i=0; i<lon; i++){
        if(palabra[i]== '_'){
            aux=aux+1;
        }
    }
    return aux;
}
void juego(usuario j[], int ele, int adiv, int errores){
    int auxerr=0;
    int vacios=0;
    do{
        system("cls");
        nombreJuego();
    switch(errores){
        case 0:ahorcado0();break;
        case 1:ahorcado1();break;
        case 2:ahorcado2();break;
        case 3:ahorcado3();break;
        case 4:ahorcado4();break;
    }
    printf("\n\n\n");
    mostrarPalabra(j[adiv], j[adiv]->longitud);
    auxerr = buscarYReemplazar(j, ele, adiv);
    if(auxerr == -1){
        errores = errores + 1;
    }
    if(errores>4){
        system("cls");
        ahorcado5();
        printf("\n\n");
        system("pause");
    }
    vacios = contarVacios(j[adiv]->palabra, j[adiv]->longitud);
    }while((errores<5) && (vacios>0));

    system("cls");

    if(errores>4){
        printf(" perdistes la palabra era %s\n", getPalabra(j[ele]));
        printf(" ganador: %s\n", getNombre(j[ele]));
        cargarPuntaje(j[ele]);
        mostrarPuntaje(j);
    }else{
        printf(" acertastes la palabra era %s\n", getPalabra(j[ele]));
        printf(" tuvistes %d errores\n", errores);
        printf(" ganador: %s\n", getNombre(j[adiv]));
        cargarPuntaje(j[adiv]);
        mostrarPuntaje(j);
    }
    printf("\n\n");
    system("pause");
}
void cargarPuntaje(usuario j){
    setPuntaje(j, (j->puntaje+1));
}
void mostrarPuntaje(usuario j[]){

    printf("   %s          %s   \n", getNombre(j[0]), getNombre(j[1]));
    printf("______________|_______________\n");
    printf("   %.2d         |  %.2d  \n", getPuntaje(j[0]), getPuntaje(j[1]));
    printf("              |        \n");

}
void puntajeFinal(usuario j[]){

    mostrarPuntaje(j);

    if(j[0]->puntaje > j[1]->puntaje){
        printf("\n\n El GANADOR ES: %s", getNombre(j[0]));
    }
     if(j[0]->puntaje < j[1]->puntaje){
        printf("\n\n El GANADOR ES: %s", getNombre(j[1]));
    }
     if(j[0]->puntaje == j[1]->puntaje){
        printf("\n\n EMPATE!!!\n PUNTAJE : %d", getPuntaje(j[0]));
    }
}
int deseaJugar(){
    char auxi;
    int aux=-1;
    do{
        system("cls");
    printf("\n desea jugar de nuevo?\n");
    printf(" 1- si \n 2- no \n");
    scanf("%c",&auxi);
    switch(auxi){
            case '1': aux=1;
                     break;
            case '2': aux=2;
                    break;
            default: printf("\n\n NO ES UN DATO VALIDO\n");
                    aux = -1;
                    break;
    }
    }while(aux == -1);

    return aux;
}
void transformarAMinuscula(char palabra[], int lon){

   char letra;
	int i=0;
    for( i=0; i<lon; i++){
        letra=palabra[i];
        switch(letra){
    case 'A':palabra[i]='a';break;
    case 'B':palabra[i]='b' ;break;
    case 'C':palabra[i]='c' ;break;
    case 'D':palabra[i]='d' ;break;
    case 'E':palabra[i]='e' ;break;
    case 'F':palabra[i]='f' ;break;
    case 'G':palabra[i]='g' ;break;
    case 'H':palabra[i]='h' ;break;
    case 'I':palabra[i]='i' ;break;
    case 'J':palabra[i]='j' ;break;
    case 'K':palabra[i]='k' ;break;
    case 'L':palabra[i]='l' ;break;
    case 'M':palabra[i]='m' ;break;
    case 'N':palabra[i]='n' ;break;
    case 'O':palabra[i]='o' ;break;
    case 'P':palabra[i]='p' ;break;
    case 'Q':palabra[i]='q' ;break;
    case 'R':palabra[i]='r' ;break;
    case 'S':palabra[i]='s' ;break;
    case 'T':palabra[i]='t' ;break;
    case 'U':palabra[i]='u' ;break;
    case 'V':palabra[i]='v' ;break;
    case 'W':palabra[i]='w' ;break;
    case 'X':palabra[i]='x' ;break;
    case 'Y':palabra[i]='y' ;break;
    case 'Z':palabra[i]='z' ;break;
        default: ;break;
        }
    }
}

void nombreJuego(){

    printf("        _       _   _  _  _       _  \n");
    printf("        _| |_| | | |  |   _|  _| | | \n");
    printf("       |_| | | |_| |  |_ |_| |_| |_| \n");
    printf("        _ _ _ _ _ _ _ _ _ _ _ _ _ _  \n");
    printf("        _ _ _ _ _ _ _ _ _ _ _ _ _ _  \n");

}
void ahorcado0(){

    printf("     _____________________________\n");
    printf("    |                ______       |\n");
    printf("    |                |     |      |\n");
    printf("    |              __|     |      |\n");
    printf("    |             |               |\n");
    printf("    |           __|               |\n");
    printf("    |          |                  |\n");
    printf("    |        __|                  |\n");
    printf("    |       |                     |\n");
    printf("    |     __|                     |\n");
    printf("    |     |                       |\n");
    printf("    |  __|                        |\n");
    printf("    | |                           |\n");
    printf("    |_|                           |\n");
    printf("    |_____________________________|\n");
}
void ahorcado1(){

    printf("     _______________________________\n");
    printf("    |                 ______        |\n");
    printf("    |                 |     |       |\n");
    printf("    |               __|   __|__     |\n");
    printf("    |             |     | _ _ |     |\n");
    printf("    |           __|     |  |  |     |\n");
    printf("    |          |        |__o__|     |\n");
    printf("    |        __|                    |\n");
    printf("    |       |                       |\n");
    printf("    |     __|                       |\n");
    printf("    |    |                          |\n");
    printf("    |  __|                          |\n");
    printf("    | |                             |\n");
    printf("    |_|                             |\n");
    printf("    |                               |\n");
    printf("    |_______________________________|\n");
}
void ahorcado2(){
    printf("     _______________________________\n");
    printf("    |                ______         |\n");
    printf("    |                |     |        |\n");
    printf("    |              __|   __|__      |\n");
    printf("    |             |     | _ _ |     |\n");
    printf("    |           __|     |  |  |     |\n");
    printf("    |          |        |__o__|     |\n");
    printf("    |        __|           |        |\n");
    printf("    |       |              |        |\n");
    printf("    |     __|              |        |\n");
    printf("    |    |                 |        |\n");
    printf("    |  __|                 |        |\n");
    printf("    | |                             |\n");
    printf("    |_|                             |\n");
    printf("    |                               |\n");
    printf("    |_______________________________|\n");
}
void ahorcado3(){
    printf("     _______________________________\n");
    printf("    |                ______         |\n");
    printf("    |                |     |        |\n");
    printf("    |              __|   __|__      |\n");
    printf("    |             |     | _ _ |     |\n");
    printf("    |           __|     |  |  |     |\n");
    printf("    |          |        |__o__|     |\n");
    printf("    |        __|       |   |   |    |\n");
    printf("    |       |          |___|___|    |\n");
    printf("    |     __|              |        |\n");
    printf("    |    |                 |        |\n");
    printf("    |  __|                 |        |\n");
    printf("    | |                             |\n");
    printf("    |_|                             |\n");
    printf("    |                               |\n");
    printf("    |_______________________________|\n");

}
void ahorcado4(){

     printf("     _______________________________\n");
    printf("     |                ______         |\n");
    printf("     |                |     |        |\n");
    printf("     |              __|   __|__      |\n");
    printf("     |             |     | _ _ |     |\n");
    printf("     |           __|     |  |  |     |\n");
    printf("     |          |        |__-__|     |\n");
    printf("     |        __|       |   |   |    |\n");
    printf("     |       |          |___|___|    |\n");
    printf("     |     __|              |        |\n");
    printf("     |    |                 |        |\n");
    printf("     |  __|               __|__      |\n");
    printf("     | |                 |     |     |\n");
    printf("     |_|                 |     |     |\n");
    printf("     |                  _|     |_    |\n");
    printf("     |_______________________________|\n");

}
void ahorcado5(){

    printf("     _______________________________\n");
    printf("    |                ______         |\n");
    printf("    |                |     |        |\n");
    printf("    |              __|   __|__      |\n");
    printf("    |             |     | x x |     |\n");
    printf("    |           __|     |  |  |     |\n");
    printf("    |          |        |__-__|     |\n");
    printf("    |        __|           |        |\n");
    printf("    |       |           ___|___     |\n");
    printf("    |     __|          |   |   |    |\n");
    printf("    |    |             |   |   |    |\n");
    printf("    |  __|               __|__      |\n");
    printf("    | |                 |     |     |\n");
    printf("    |_|                 |     |     |\n");
    printf("    |                  _|     |_    |\n");
    printf("    |___________________ ___________|\n");

}
